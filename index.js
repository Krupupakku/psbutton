const images = [
    "./buttons/PScrossWhite.png",
    "./buttons/PSsquareWhite.png",
    "./buttons/PScircleWhite.png",
    "./buttons/PStriangleWhite.png"
]

let firstImage = document.createElement("img"),
    secondImage = document.createElement("img"),
    thirdImage = document.createElement("img"),
    fourthImage = document.createElement("img");

let orderedImages = images.sort(() => Math.random() - 0.5);

firstImage.src = orderedImages[0];
secondImage.src = orderedImages[1];
thirdImage.src = orderedImages[2];
fourthImage.src = orderedImages[3];

let container = document.getElementById('container')

container.appendChild(firstImage);
container.appendChild(secondImage);
container.appendChild(thirdImage);
container.appendChild(fourthImage);